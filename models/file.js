const mongoose = require('mongoose');

var FileSchema = mongoose.Schema({
  path: {
    type: String
  },
  name: {
    type: String,
  },
  type: {
    type: String
  },
  owner: {
    type: String
  },
  thumb: {
    type: String
  }
});

var File = module.exports = mongoose.model('File', FileSchema);

module.exports.createFile = function(newFile, callback) {
    newFile.save(callback);
}

module.exports.getFiles = function(userId, callback) {
  File.find({owner: userId}, callback);
}

module.exports.getFile = function(userId, fileId, callback) {
  File.findOne({owner: userId, _id: fileId}, callback);
}
