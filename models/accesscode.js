const mongoose = require('mongoose');

var accessCodeSchema = mongoose.Schema({
  code: {
    type: String
  },
  isValid: {
    type: Number
  }
});

var accessCode = module.exports = mongoose.model('AccessCode', accessCodeSchema);

// Get code
module.exports.getCode = function(code, callback) {
  var query = {code: code};

  accessCode.findOne(query, callback);
}

// Create code
module.exports.createCode = function(newCode, callback) {
  newCode.save(callback);
}


module.exports.isValid = function(code, callback) {
  var query = {code: code};

  accessCode.findOne(query, function(err, code) {
    if (err) throw err;

    if (code == null || code.isValid == null) {
      callback(null, 0);
    }
    else {
      if (code.isValid == 1) {
        callback(null, 1);
      } else {
        callback(null, 0);
      }
    }
  });
}
