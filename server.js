// Node
var path = require('path');
// express
var express = require('express');
var app = express();
var flash = require('connect-flash');
// parser's
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
// Session & auth
var session = require('express-session');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
// Db
var mongo = require('mongodb').MongoClient;
var mongoose = require('mongoose');

//Routes
var indexRoutes = require('./routes/index');
var authRoutes = require('./routes/auth');

// Fix open() deprecation
var mongoOptions = {
  useMongoClient: true,
  reconnectTries: 10
};

mongoose.connect('mongodb://localhost/kecafiles', mongoOptions);
var db = mongoose.connection;

//TODO: configure '/' index route

// App config
app.set('views', path.join(__dirname + "/views"));
app.set('view engine', 'pug');
app.use('/static', express.static('static'));
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Express session
app.use(session({
  secret: 'secret',
  saveUninitialized: true,
  resave: true
}));

// Passport init
app.use(passport.initialize());
app.use(passport.session());

// init flash messages
app.use(flash());

app.use(function(req, res, next) {
  res.locals.success = req.flash('success');
  res.locals.error = req.flash('error');

  return next();
});

// auth middleware
app.use('/dash', function (req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }
  else {
    req.flash('error', 'You need to be logged in');
    res.redirect('/auth/login');
  }
});

// Routes
app.use('/', indexRoutes);
app.use('/auth', authRoutes);

app.listen(3000, function() {
  console.log('Listening on port 3000');
});
