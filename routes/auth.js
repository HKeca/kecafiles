var express = require('express');
var router = express.Router();
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

// User model
var User = require('../models/user');
// access code model
var AccessCode = require('../models/accesscode');

// Login view
router.get('/login', function(req, res) {
  res.render('login.pug');
});

// Register view
router.get('/register', function(req, res) {
  res.render('register.pug');
});

// Register function
router.post('/register', function(req, res) {
  var userName = req.body.username;
  var passWord = req.body.password;
  var accessCode = req.body.accesscode;


  AccessCode.isValid(accessCode, function(err, isValid) {
      if (isValid == 1) {

        var newUser = new User({
          username: userName,
          password: passWord,
          isAdmin: false
        });

        User.createUser(newUser, function(err, user) {
          if(err) throw err;
        });

        req.flash('success', 'You have been registered');
        res.redirect('/auth/login');
      }
      else
      {
        req.flash('error', 'Access Code is invalid');
        res.redirect('/auth/register');
      }
  });
});

// Passport login
passport.use(new LocalStrategy(
  function(username, password, done) {
    User.getUser(username, function(err, user) {
      if (err) throw err;

      if (!user) {
        return done(null, false, {message: 'Invalid Username / Password'});
      }

      User.comparePassword(password, user.password, function(err, isMatch) {
        if (err) throw err;

        if (isMatch) {
          return done(null, user);
        } else {
          return done(null, false, {message: 'Invalid Username / Password'})
        }
      });
    });
  }
));

// Serialize
passport.serializeUser(function(user, done) {
  done(null, user.id)
});

passport.deserializeUser(function(id, done) {
  User.getUserById(id, function(err, user) {
    done(err, user);
  });
});

// Login function
router.post('/login',
  passport.authenticate('local', {successRedirect: '/dash', failureRedirect: '/auth/login'}),

  function(req, res) {
    res.redirect('/dash');
});

// Logout
router.get('/logout', function(req, res) {
  req.logout();
  req.flash('success', 'You have been logged out!');

  res.redirect('/auth/login');
});

module.exports = router;
