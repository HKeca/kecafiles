var express = require('express');
var router = express.Router();
var multer = require('multer');
var fs = require('fs');

var upload = multer({ dest: __dirname + '/../files/' });

var File = require('../models/file');


// Home
router.get('/dash', function(req, res) {

  var owner = req.user.id;


  var files = File.getFiles(owner, function(err, myFiles) {
    if (err) throw err;

    res.render('index.pug', {files: myFiles});
  });
});

// My Account
router.get('/dash/myaccount', function(req, res) {

  var user = {
    username: req.user.username
  };

  res.render('account.pug', {userData: user});
});

// File uploads
router.post('/dash/upload', upload.single('userFile'), function(req, res) {
  var user = req.user.id;

  var newFile = new File({
    name: req.file.originalname,
    path: req.file.path,
    owner: user,
    type: req.file.mimetype,
    thumb: 'https://placehold.it/200x200'
  });

  var createFile = File.createFile(newFile, function(err, file) {
    if (err) {
      req.flash('error', 'File could not be uploaded');
      res.redirect('/dash');
    }

    req.flash('success', 'File Uploaded');
    res.redirect('/dash');
  });
});

// Download file
router.get('/dash/download/:id', function(req, res) {
    var userId = req.user.id;
    var fileId = req.params.id;

    var myFile = File.getFile(userId, fileId, function(err, file) {
      if (err) {
        req.flash('error', " " + err);
        res.redirect('/dash');
      }

      var down = file.path;
      res.download(down, file.name);
    });
});

// Delete file
router.get('/dash/remove/:id', function(req, res) {
  var userId = req.user.id;
  var fileId = req.params.id;

  var file = File.getFile(userId, fileId, function(err, file) {
    if (err) throw err;

    fs.unlink(file.path, function(err) {
      if (err) throw err;
    });

    file.remove();

    req.flash('success', 'File was removed');
    res.redirect('/dash');
  });
});

module.exports = router;
