setTimeout(function() {
  var uploadModal = document.getElementById('upload-container');
  var uploadBtn = document.getElementById('upload-btn');
  var closeBtn = document.getElementById('close-upload');

  if (uploadBtn) {

    uploadBtn.addEventListener('click', function(event) {
      event.preventDefault();

      uploadModal.style.display = "flex";
    });
  }

  if (closeBtn) {
    closeBtn.addEventListener('click', function(event) {
      event.preventDefault();
  
      uploadModal.style.display = "none";
    });
  }


  var close = document.querySelector('.notify');

  if (close) {
    setTimeout(function() {
        var notify = document.querySelector('.notify');
        notify.style.width = "100%";
    }, 300);


    close.addEventListener('click', function(event) {
        event.preventDefault();
        close.remove();
    });
  }



}, 100);
