var gulp = require('gulp');
var sass = require('gulp-sass');
var useref = require('gulp-useref');


gulp.task('styles', function() {
  gulp.src('styles/main.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(useref())
    .pipe(gulp.dest('./static/css'));
});

gulp.task('default', function() {
  gulp.watch('styles/**/*.scss', ['styles']);
});
